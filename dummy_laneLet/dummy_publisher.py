import rclpy
from geometry_msgs.msg import Point
from std_msgs.msg import Header
from lanelet_msgs.msg import Lanelet,LineString

def create_point(x_axe,y_axe,z_axe):
    point=Point()
    point.x=x_axe
    point.y=y_axe
    point.z=z_axe
    return point

def create_lanelet(points1:Point,points2:Point):
    data=Lanelet()
    pass

class Dummy:
    def __init__(self,node,publisher) -> Dummy:
        self.node=node
        self.message_pub = publisher
        self.node.get_logger().info('Hi from dummy_laneLet.')

    def publish(self) -> None:
        data=Lanelet()
        for i in range(10):
            data.left_boundary.line_string.append(create_point(float(i * 10),float(-20),float(0)))
            data.right_boundary.line_string.append(create_point(float(i * 10),float(20),float(0)))
        self.message_pub.publish(data)



def main():
    rclpy.init()
    node=rclpy.create_node("LaneLet_publisher")
    message_pub = node.create_publisher(Lanelet,"target_pose", 10)
    dummy=Dummy(node,message_pub)
    while rclpy.ok():
        dummy.publish()


if __name__ == '__main__':
    main()
