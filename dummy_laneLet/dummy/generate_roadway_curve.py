import numpy as np

def generate_roadway_curve(radius, lane_width=0.4, num_points=100):
    """
    Generate two point lists representing a curved roadway with a given radius
    and lane width, where the car drives in the middle of the lane.

    :param radius: The radius of the curve (positive for right curve, negative for left).
    :param lane_width: The width of the lane in meters (default is 0.4 meters).
    :param num_points: The number of points to generate along the curve (default is 100).
    :return: Two lists of (x, y) coordinates for the left and right lanes.
    """
    if radius == 0:
        raise ValueError("Radius cannot be zero.")

    # Calculate the angle step between points
    angle_step = np.deg2rad(360 / num_points)

    # Calculate the coordinates of the center of the curve
    center_x = 0.0
    center_y = abs(radius)

    # Initialize lists to store left and right lane points
    left_lane = []
    right_lane = []

    for i in range(num_points):
        # Calculate the angle for the current point
        angle = i * angle_step

        # Calculate the x and y coordinates for the point on the curve
        x = center_x + radius * np.sin(angle)
        y = center_y - radius * np.cos(angle)

        # Calculate the coordinates for the left and right lanes
        left_x = x - (lane_width / 2)
        right_x = x + (lane_width / 2)

        # Add the coordinates to the respective lists
        left_lane.append((left_x, y))
        right_lane.append((right_x, y))

    return left_lane, right_lane

# Example usage:
radius = 10.0  # Example radius (positive for right curve)
left_lane, right_lane = generate_roadway_curve(radius)
print(f"left_lane:{left_lane}\n{right_lane}")
# You can now use left_lane and right_lane lists for further processing or visualization.
